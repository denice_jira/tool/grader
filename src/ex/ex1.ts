import {
  ANS,
  AuthLocation,
  AuthType,
  HttpMethod,
  Matcher,
  Type,
} from '../types/ans.type';

const ans: ANS = {
  name: 'exercise basic 101',
  symbol: 'EX1',
  description: 'babababababa',
  listHide: false,
  checkList: [
    {
      name: 'Prepare 1',
      description: 'Clear data',
      type: Type.PREPARE,
      point: 1,
      request: {
        method: HttpMethod.GET,
        endpoint: '/grader/clear',
      },
      response: {
        body: [
          {
            matcher: Matcher.EQUAL,
            value: 'OK',
          },
        ],
      },
    },
    {
      name: 'Prepare 2',
      description: 'Seed data',
      type: Type.PREPARE,
      point: 1,
      request: {
        method: HttpMethod.GET,
        endpoint: '/seed',
      },
      response: {
        body: [
          {
            matcher: Matcher.EQUAL,
            value: 'OK',
          },
        ],
      },
    },
    {
      name: 'Prepare 3',
      description: 'Create new user',
      type: Type.PREPARE,
      point: 1,
      request: {
        method: HttpMethod.POST,
        endpoint: '/user',
        body: {
          username: 'grader',
          password: '123456',
        },
      },
      response: {
        body: [
          {
            matcher: Matcher.EQUAL,
            value: 'OK',
          },
        ],
      },
    },
    {
      name: 'Prepare 4',
      description: 'Get admin pass',
      type: Type.PREPARE,
      request: {
        method: HttpMethod.GET,
        endpoint: '/grader/user',
      },
      adv: {
        after: {
          func: `myfunc = async function(input) {for(let i=0;i<input.body.length;i++){if(input.body[i].username==='admin@admin.com'){storage['adminPass']=input.body[i].password}}}`,
        },
      },
      response: {
        body: [],
      },
    },
    {
      name: 'Prepare 4.5',
      description: 'admin login',
      type: Type.PREPARE,
      request: {
        method: HttpMethod.POST,
        endpoint: '/login',
        body: {
          username: 'admin@admin.com',
          password: '123456',
        },
      },
      adv: {
        before: {
          func: `myfunc = async function(input) {input.body.password = storage['adminPass']; return input;}`,
        },
        after: {
          func: `myfunc = async function(input) {storage['adminToken']=input.body.access_token}`,
        },
      },
      response: {
        body: [
          {
            matcher: Matcher.NOT_EMPTY,
            value: 'access_token',
          },
          {
            matcher: Matcher.IS_JSON,
          },
        ],
      },
    },
    {
      name: 'Prepare 5',
      description: 'Login',
      type: Type.PREPARE,
      point: 1,
      adv: {
        after: {
          func: `myfunc = async function(input) {storage['userToken']=input.body.access_token}`,
        },
      },
      request: {
        method: HttpMethod.POST,
        endpoint: '/login',
        auth: {
          localtion: AuthLocation.BODY,
          key: 'access_token',
        },
        body: {
          username: 'grader',
          password: '123456',
        },
      },
      response: {
        body: [
          {
            matcher: Matcher.NOT_EMPTY,
            value: 'access_token',
          },
          {
            matcher: Matcher.IS_JSON,
          },
        ],
      },
    },
    {
      name: 'Check 1',
      description: 'Login wrong',
      type: Type.CHECK,
      point: 1,
      request: {
        method: HttpMethod.POST,
        endpoint: '/login',
        body: {
          username: 'grader',
          password: 'xxxxxx',
        },
      },
      response: {
        body: [
          {
            matcher: Matcher.EQUAL,
            value: 'ERROR',
          },
        ],
      },
    },
    {
      name: 'Check 2',
      description: 'adv',
      type: Type.TMP,
      point: 1,
      adv: {
        before: {
          func: `myfunc = async function(input) {console.log(input,"before");}`,
        },
        after: {
          func: `myfunc = async function(input) {console.log(input,"after");}`,
        },
      },
      request: {
        method: HttpMethod.POST,
        endpoint: '/login',
        body: {
          username: 'grader',
          password: '123456',
        },
      },
      response: {
        body: [
          {
            matcher: Matcher.EQUAL,
            value: 'ERROR',
          },
        ],
      },
    },
    {
      name: 'point 1',
      description: 'jwt',
      type: Type.POINT,
      point: 10,
      adv: {
        before: {
          func: `myfunc = async function(input) {token ='Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImdyYWRlciIsImFkbWluIjpmYWxzZSwiaWF0IjoxNjMzNTA5Mzc4LCJleHAiOjE2MzM1NDUzNzh9.W1Zxlt-6deP9UD8uwH-7j4e3KBqWboGGcChmlX4AQWk';return input;}`,
        },
      },
      request: {
        method: HttpMethod.GET,
        endpoint: '/noteList',
        requireAuth: AuthType.bearer,
      },
      response: {
        body: [
          {
            matcher: Matcher.EQUAL,
            value: {
              statusCode: 401,
              message: 'Unauthorized',
            },
          },
        ],
      },
    },
  ],
};
export default ans;
