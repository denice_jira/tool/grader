import { rawListeners } from 'process';
import ex1 from './ex/ex1';
import {
  ANS,
  AuthLocation,
  AuthType,
  CheckList,
  CheckListRequest,
  CheckListResponse,
  HttpMethod,
  Matcher,
  MatcherCheck,
  Type,
} from './types/ans.type';
import axios from 'axios';
import * as pr from 'prompt-sync';
import * as _ from 'lodash';

const prompt = pr();
const exList = {
  ex1: ex1,
};
let select: ANS = null;
const userPoint = [];
let result = 0;
let enpointUrl = 'http://127.0.0.1:3000';
let token = '';
const storage = {};
async function evalExec(input, evalString) {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  // eslint-disable-next-line prefer-const
  let myfunc = function (input) {
    return input;
  };
  eval(evalString);
  return await myfunc(input);
}
async function authToken(
  request: CheckListRequest,
  response: CheckListResponse,
) {
  if (request.auth.localtion === AuthLocation.BODY) {
    token = response.body[request.auth.key];
  }
}
async function apiCall(url, method, body = undefined, header = undefined) {
  const result: CheckListResponse = {};
  if (method === HttpMethod.GET) {
    try {
      const response = await axios.get(enpointUrl + url, { headers: header });
      result.body = response.data;
      result.status = response.status;
      result.header = response.headers;
    } catch (error) {
      result.body = error.response.data;
      result.status = error.response.status;
      result.header = error.response.headers;
    }
  }
  if (method === HttpMethod.POST) {
    try {
      const response = await axios.post(enpointUrl + url, body, {
        headers: header,
      });
      result.body = response.data;
      result.status = response.status;
      result.header = response.headers;
    } catch (error) {
      result.body = error.response.data;
      result.status = error.response.status;
      result.header = error.response.headers;
    }
  }
  return result;
}
async function checkMatch(result, expect: MatcherCheck[]) {
  const { body } = result;
//   console.log('result', result);
//   console.log('expect', expect);
  let check = true;
  for (let i = 0; i < expect.length; i++) {
    if (typeof body === 'string') {
      if (expect[i].matcher === Matcher.IS_JSON) {
        check = false;
      }
      if (expect[i].matcher === Matcher.NOT_EMPTY) {
        if (body === null || body === '' || body === undefined) {
          check = false;
        }
      }
      if (expect[i].matcher === Matcher.EQUAL) {
        if (body !== expect[i].value) {
          check = false;
        }
      }
    } else {
      if (expect[i].matcher === Matcher.NOT_EMPTY) {
        if (
          body[expect[i].value as string] === null ||
          body[expect[i].value as string] === '' ||
          body[expect[i].value as string] === undefined
        ) {
          check = false;
        }
      }
      if (expect[i].matcher === Matcher.EQUAL) {
        // eslint-disable-next-line @typescript-eslint/ban-types
        if (!(_.isArray(expect[i].value as object) === _.isArray(body))) {
          check = false;
        }
        // eslint-disable-next-line @typescript-eslint/ban-types
        if (!_.isMatch(expect[i].value as object, body)) {
          check = false;
        }
      }
    }
  }
  return check;
}
async function runTask(task: CheckList) {
  console.log(`       Running [${task.name}] ...`);
  console.log(`                ${task.description}`);
  if (task.adv) {
    if (task.adv.before) {
      task.request = await evalExec(task.request, task.adv.before.func);
    }
  }
  if (task.request.requireAuth === AuthType.bearer) {
    if (task.request.header === undefined) task.request.header = {};
    task.request.header['Authorization'] = token;
  }
  const response: CheckListResponse = await apiCall(
    task.request.endpoint,
    task.request.method,
    task.request.body,
    task.request.header,
  );
  if (task.request.auth) {
    await authToken(task.request, response);
  }
  if (await checkMatch(response, task.response.body)) {
    userPoint.push({
      task: task,
      result: response,
      point: task.point,
    });
    if (task.adv) {
      if (task.adv.after) {
        await evalExec(response, task.adv.after.func);
      }
    }
    console.log(`       Result Success`);
  } else {
    userPoint.push({
      task: task,
      result: response,
      point: 0,
    });
    console.log(`       Result Fail`);
    console.log(`           Request:`, task.request);
  }
}
async function prepare() {
  for (let i = 0; i < select.checkList.length; i++) {
    if (select.checkList[i].type === Type.PREPARE) {
      await runTask(select.checkList[i]);
    }
  }
}
async function check() {
  for (let i = 0; i < select.checkList.length; i++) {
    if (select.checkList[i].type === Type.CHECK) {
      await runTask(select.checkList[i]);
    }
  }
}
async function point() {
  for (let i = 0; i < select.checkList.length; i++) {
    if (select.checkList[i].type === Type.POINT) {
      await runTask(select.checkList[i]);
    }
  }
}
async function report() {
  let sum = 0;
  let total = 0;
  for (let i = 0; i < userPoint.length; i++) {
    if (typeof userPoint[i].point === 'number') {
      sum = sum + userPoint[i].point;
    }
  }
  for (let i = 0; i < select.checkList.length; i++) {
    if (select.checkList[i].type === Type.TMP) continue;
    if (typeof select.checkList[i].point === 'number') {
      total = total + select.checkList[i].point;
    }
  }
  console.log(`=====================================================`);
  console.log(`                       Report                        `);
  console.log(`=====================================================`);
  console.log(`   Score = ${sum}/${total}`);
  await encryptReport(sum);
}
async function encryptReport(score) {
  const os = require('os');
  const hostname = os.hostname();
  const message = `hostname_${hostname}|score_${score}`;
  const secret = `${select.symbol}_`;
  const crypto = require('crypto');
  const hash = crypto
    .createHmac('sha256', secret)
    .update(message)
    .digest('hex');
  console.log(`   Submmit code = ${message}|${hash}`);
}
async function flow() {
  console.log(`   > Preparing data`);
  await prepare();
  console.log(`   > Checking service`);
  await check();
  console.log(`   > Scoring user`);
  await point();
  console.log(`=====================================================`);
  await report();
}
async function start() {
  console.log(`=====================================================`);
  console.log(`                   Grader Exercise                   `);
  console.log(`                  Created by Denice                  `);
  console.log(`=====================================================`);
  const ex = prompt(`Select Exercise [${Object.keys(exList)}]: `);
  if (exList[ex] === null || exList[ex] === undefined) {
    result = -1;
    return;
  }
  const enpoint = prompt(`Enter Endpoint [127.0.0.1:3000]:`);
  if (enpoint !== '') {
    enpointUrl = enpoint;
  }
  select = exList[ex];
  await flow();
  result = 0;
  return;
}

do {
  console.clear();
  start();
  if (result !== 0) {
    const restart = prompt(`press r to restart: `);
    result = restart === 'r' ? -1 : 0;
  }
} while (result !== 0);
