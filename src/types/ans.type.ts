export interface ANS {
  name: string;
  symbol: string;
  description: string;
  listHide: boolean;
  checkList: CheckList[];
}
export enum Type {
  PREPARE = 'PREPARE',
  CHECK = 'CHECK',
  POINT = 'POINT',
  TMP = 'TMP',
}
export enum HttpMethod {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
}
export enum Matcher {
  EQUAL,
  CONTAIN,
  PARTIAL,
  OR_EQUAL,
  OR_CONTAIN,
  NOT,
  NOT_EMPTY,
  IS_JSON,
}

export interface MatcherCheck {
  matcher: Matcher;
  value?: string | unknown;
}
export enum AuthLocation {
  BODY,
  HEADER,
}
export enum AuthType {
  bearer,
}
export interface Auth {
  localtion: AuthLocation;
  key: string;
}
export interface CheckListRequest {
  endpoint: string;
  method: HttpMethod;
  header?: unknown;
  body?: unknown;
  auth?: Auth;
  requireAuth?: AuthType;
}

export interface CheckListResponse {
  header?: unknown;
  body?: MatcherCheck[];
  status?: number;
}
export interface EvalFunction {
  func: string;
}
export interface Adv {
  before?: EvalFunction;
  after?: EvalFunction;
}
export interface CheckList {
  name: string;
  description: string;
  type: Type;
  request: CheckListRequest;
  response: CheckListResponse;
  matcher?: Matcher;
  point?: number;
  adv?: Adv;
}
